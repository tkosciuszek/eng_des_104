from time import sleep

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
def rc_time(pin_to_circuit=4):
    '''
    :param pin_to_circuit:
    :return count value which can be used to determine amount of pressure on sensor:
    '''
    count = 0

    # Output on the pin for
    GPIO.setup(pin_to_circuit, GPIO.OUT)
    GPIO.output(pin_to_circuit, GPIO.LOW)
    sleep(0.05)

    # Change the pin back to input
    GPIO.setup(pin_to_circuit, GPIO.IN)

    # Count until the pin goes high
    while (GPIO.input(pin_to_circuit) == GPIO.LOW):
        count += 1

    return count


x = rc_time()

print(x)
