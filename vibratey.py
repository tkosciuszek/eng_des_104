import time

# from importlib import util
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
# try:
#     util.find_spec('RPi.GPIO')
#     import RPi.GPIO as GPIO
# except ImportError:
#     """
#     import FakeRPi.GPIO as GPIO
#     OR
#     import FakeRPi.RPiO as RPiO
#     """
#
#     import FakeRPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)


def vibe(reps=4, vibe_pin=21):
    # Set value of number of repeats
    counter = reps
    # This pin is an output pin

    GPIO.setup(vibe_pin, GPIO.OUT)

    # Repeat this action while the statement is true
    while counter > 0:
        # Turn the pin on to 3V3
        GPIO.output(vibe_pin, GPIO.HIGH)

        # Time.sleep will make the system wait for x second(s)
        time.sleep(1)

        # Turn the pin on to 3V3
        GPIO.output(vibe_pin, GPIO.LOW)

        # Wait another x second(s)
        if counter != 1:
            time.sleep(0.2)

        counter -= 1


vibe(4)
