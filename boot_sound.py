from time import sleep

import RPi.GPIO as GPIO
from omxplayer.player import OMXPlayer

# from pathlib import Path
GPIO.setmode(GPIO.BCM)


def play_sound(sound_location='/home/pi/eng_des_104/Boot_Sound.mp3'):
    '''
    :param file_location:
    Actuates the playing of the sound specified in the file location listed in the function input
    :return None:
    '''
    # path = Path()
    # audio = MP3(sound_location)
    # song_length = int(audio.info.length)

    sound_pin = 23
    GPIO.setup(sound_pin, GPIO.OUT)

    GPIO.output(sound_pin, GPIO.HIGH)
    # Need a command to wake up amp

    # Command to play song (note that omxplayer may not work)
    player = OMXPlayer(sound_location, args='-o local')

    # Time.sleep will make the system wait for x second(s)
    sleep(10)

    player.quit()

    GPIO.output(sound_pin, GPIO.LOW)

    # Turn off the amp (inverse of above)
    # GPIO.output(sound_pin, GPIO.LOW)


play_sound()
