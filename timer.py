import datetime
import os.path
import time

import RPi.GPIO as GPIO
# from subprocess import run
from omxplayer.player import OMXPlayer

processing = 0

GPIO.setmode(GPIO.BCM)


def vibe(reps=4, vibe_pin=21, thresh=150):
    # Set value of number of repeats
    counter = reps
    # This pin is an output pin

    GPIO.setup(vibe_pin, GPIO.OUT)

    # Repeat this action while the statement is true
    while counter > 0:
        if rc_time() > thresh:
            return None
        # Turn the pin on to 3V3
        GPIO.output(vibe_pin, GPIO.HIGH)

        # Time.sleep will make the system wait for x second(s)
        time.sleep(1)

        # Turn the pin on to 3V3
        GPIO.output(vibe_pin, GPIO.LOW)

        # Wait another x second(s)
        if counter != 1:
            time.sleep(0.2)

        counter -= 1


def play_sound(sound_location='/home/pi/eng_des_104/Cushion_Sound.mp3'):
    '''
    :param file_location:
    Actuates the playing of the sound specified in the file location listed in the function input
    :return None:
    '''
    # path = Path()
    # audio = MP3(sound_location)
    # song_length = int(audio.info.length)

    sound_pin = 23
    GPIO.setup(sound_pin, GPIO.OUT)

    # wakes up the amp
    GPIO.output(sound_pin, GPIO.HIGH)

    # Command to play song (note that omxplayer may not work)
    player = OMXPlayer(sound_location, args='-o local')
    # run(['aplay', sound_location])

    # Time.sleep will make the system wait for x second(s)
    time.sleep(10)

    # player.quit()

    GPIO.output(sound_pin, GPIO.LOW)

    # Turn off the amp (inverse of above)
    # GPIO.output(sound_pin, GPIO.LOW)


def rc_time(pin_to_circuit=4):
    '''
    :param pin_to_circuit:
    :return count value which can be used to determine amount of pressure on sensor:
    '''
    count = 0

    # Output on the pin for
    GPIO.setup(pin_to_circuit, GPIO.OUT)
    GPIO.output(pin_to_circuit, GPIO.LOW)
    time.sleep(0.05)

    # Change the pin back to input
    GPIO.setup(pin_to_circuit, GPIO.IN)

    # Count until the pin goes high
    while (GPIO.input(pin_to_circuit) == GPIO.LOW):
        count += 1

    return count


def sit_counter(num_mins=0.5, freq=12, thresh=150, stand_thresh=15):
    '''
        :param num_mins: integer defining the number of minutes required to activate sensor
            thresh: the integer threshold of pressure from pressure sensor to determine "sitting"
                    (arbitrary integer value, 0 = max force, +inf "possible" with little force)
            freq: num of times per minute that the pressure sensor is checked
            stand_thresh: the amount of time (int in secs) the user must remain standing before the
                reminder timer cancels the sitting event

        :return returns nothing, activates vibrator and speaker upon sitting too long event:
    '''
    # global processing

    # find a way to get system time from the raspberry pi

    # haha might not need the system time :((((

    # questions relating to the functioning of this code on the pi:
    # if we use cron scheduler, will it execute a presscheck each minute and then potentially
    # fire this code multiple times?

    # what happens to cron scheduler if this code is already (in the middle of) executing?
    current_time = datetime.datetime.now()
    hour = current_time.strftime('%H')
    # print(current_time)
    # print(hour)
    quiet_hours = [0, 1, 2, 3, 4, 5, 6, 22, 23, 24]
    # if processing == 1:
    if os.path.isfile("sitting.txt") or (hour in quiet_hours):
        print('Sitting file exists still. Iteration Terminated. Function already executing.')
        return None

    elif rc_time() < thresh:
        # processing = 1
        open("sitting.txt", "x")
        time_dur = 0

        # determine the length of time that someone has been sitting
        # uses 5 second intervals
        for interval in range(int(num_mins * freq)):
            pressure = rc_time()

            # person has stood up if > 150
            if pressure > thresh:
                stand_count = 0
                for interval_2 in range(int(stand_thresh)):
                    pressure_2 = rc_time()
                    # still standing
                    if pressure_2 > thresh:
                        stand_count += 1
                    else:
                        # break the inner for loop and go back to checking duration
                        # of outer for loop
                        break
                    time.sleep(1)
                    time_dur += 1

                # stand up in final interval of total time minus stand threshold
                if (num_mins * 60 - stand_thresh) < time_dur and stand_count >= stand_thresh:
                    # processing = 0
                    print('Function terminated, user standing for too long')
                    os.remove("sitting.txt")
                    return None

                # stand up before total time minus stand threshold
                elif (num_mins * 60 - stand_thresh) >= time_dur and stand_count >= stand_thresh:
                    # processing = 0
                    print('Function terminated, user standing for too long')
                    os.remove("sitting.txt")
                    return None



            else:
                time_dur += (60 / freq)
                time.sleep(60 / freq)
                print(time_dur)

        # total time elapsed is greater than the desired sitting period
        if time_dur >= (num_mins * 60):
            while rc_time() < thresh:
                # build for loop to check for rc_time() if value goes above threshold to determine that
                # user has actually stood up
                print('Get up plz, user sitting too long')
                # moved positioning
                play_sound()
                vibe()
                # processing = 0

                if rc_time() > thresh:
                    stand_count = 0
                    for interval_2 in range(int(stand_thresh)):
                        pressure_2 = rc_time()
                        # still standing
                        if pressure_2 > thresh:
                            stand_count += 1
                        else:
                            # break the inner for loop and go back to checking duration
                            # of outer for loop
                            break
                        time.sleep(1)
                        if stand_count > stand_thresh:
                            os.remove("sitting.txt")
                            return None

            os.remove("sitting.txt")
            return None

sit_counter()
