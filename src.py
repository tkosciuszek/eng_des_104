#!/usr/local/bin/python
import importlib.util
import time

import omxplayer

try:
    importlib.util.find_spec('RPi.GPIO')
    import RPi.GPIO as GPIO
except ImportError:
    """
    import FakeRPi.GPIO as GPIO
    OR
    import FakeRPi.RPiO as RPiO
    """

    import FakeRPi.GPIO as GPIO
    # https://github.com/sn4k3/FakeRPi

GPIO.setmode(GPIO.BCM)


# note that GPIO is NOT THE SAME AS PIN
# vibration motor pin 40
# use the RTC with start time and then when time difference is enough
# define the pin that goes to the circuit

# shutdown pin on the amplifier when set to low will save power
# when set high, the speaker will run and work this is pin 16

# currently pin 16 has been set automatically to high (we need to change this)

# pin 7 for the sensor

# timing this function from activation to output will give us sensitivity. Quicker = more pressure
# Find pin detector
def rc_time(pin_to_circuit=7):
    '''
    :param pin_to_circuit:
    :return count value which can be used to determine amount of pressure on sensor:
    '''
    count = 0

    # Output on the pin for
    GPIO.setup(pin_to_circuit, GPIO.OUT)
    GPIO.output(pin_to_circuit, GPIO.LOW)
    time.sleep(0.1)

    # Change the pin back to input
    GPIO.setup(pin_to_circuit, GPIO.IN)

    # Count until the pin goes high
    while (GPIO.input(pin_to_circuit) == GPIO.LOW):
        count += 1

    return count


# Catch when script is interrupted, cleanup correctly
try:
    # Main loop which seems to continuously print the value of the pressure sensor
    while True:
        print(rc_time(pin_to_circuit))
except KeyboardInterrupt:
    pass
finally:
    GPIO.cleanup()


def set_RTC(rtc_obj):


# Does the pin need to be high if it is on?

#


def timer():
    '''
    :param none:
    :return Message upon completion of specified time to action:
    '''
    # MicroPython libraries, specifically machine.RTC() as a real time clock object

    # Wait 30 mins
    time.sleep(30 * 60)
    # return 'It's time to stand up'


def play_sound(sound_location):
    '''
    :param file_location:
    Actuates the playing of the sound specified in the file location listed in the function input
    :return None:
    '''
    song_info = sound_location.info
    song_length = int(song_info.length)

    sound_pin = 23
    GPIO.setup(sound_pin, GPIO.OUT)
    cd
    GPIO.output(sound_pin, GPIO.HIGH)
    # Need a command to wake up amp

    # Command to play song (note that omxplayer may not work)
    omxplayer
    sound_location

    # Time.sleep will make the system wait for x second(s)
    time.sleep(song_length)

    # Turn off the amp (inverse of above)
    GPIO.output(sound_pin, GPIO.LOW)


def vol_up():


# empty

def vol_down():
# empty

# for time in range(4):
#     play_sound(sound.mp3)
#     time.sleep(5)
